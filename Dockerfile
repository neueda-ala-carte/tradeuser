FROM openjdk:11.0
ADD target/mstp-userbackend-0.0.1-SNAPSHOT.jar ./app.jar
ENV MONGODB_URI=mongodb://tradedb:27017/users
EXPOSE 8222
ENTRYPOINT ["java", "-jar", "./app.jar"]
