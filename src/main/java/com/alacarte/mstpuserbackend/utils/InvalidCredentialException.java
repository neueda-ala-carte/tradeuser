package com.alacarte.mstpuserbackend.utils;

public class InvalidCredentialException extends Exception {
    public InvalidCredentialException(String message) {
        super(message);
    }
}
