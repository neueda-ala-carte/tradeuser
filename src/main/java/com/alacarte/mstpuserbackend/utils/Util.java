package com.alacarte.mstpuserbackend.utils;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

public class Util {

    // static variable to generate keys
    private static final int ITERATIONS = 10;

    // static variable to generate keys
    private static final int KEY_LENGTH = 128;

    /*
    private static String ByteArrayToString(byte[] ba) {
        StringBuilder hex = new StringBuilder(ba.length * 2);
        for (byte b : ba) {
            hex.append(String.format("%02X", b));
        }
        return hex.toString();
    }
    */

    /**
     * to generate salt
     * @return byte[]
     */
    public static byte[] generateSalt() {
        SecureRandom random = new SecureRandom();
        byte bytes[] = new byte[KEY_LENGTH];
        random.nextBytes(bytes);
        return bytes;
    }

    /**
     * to generate apiKey for market data access
     * @param uniqueString
     * @return byte[]
     */
    private static byte[] hash(char[] uniqueString, byte[] salt) {
        PBEKeySpec spec = new PBEKeySpec(uniqueString, salt, ITERATIONS, KEY_LENGTH);
        Arrays.fill(uniqueString, Character.MIN_VALUE);
        try {
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            return skf.generateSecret(spec).getEncoded();
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new AssertionError("Error while hashing: " + e.getMessage(), e);
        } finally {
            spec.clearPassword();
        }
    }

    public static String hashString(String str, String salt) {
        return toHexString(hash(str.toCharArray(), toByteArray(salt)));
    }

    public static String toHexString(byte[] array) {
        return DatatypeConverter.printHexBinary(array);
    }

    public static byte[] toByteArray(String s) {
        return DatatypeConverter.parseHexBinary(s);
    }

}
