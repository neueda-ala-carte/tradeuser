package com.alacarte.mstpuserbackend;

import lombok.Data;

@Data
public class LoginForm {
    private String email;
    private String password;

    LoginForm() {}

    LoginForm(String email, String password) {
        email = email;
        password = password;
    }

}
