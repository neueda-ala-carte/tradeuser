package com.alacarte.mstpuserbackend;

import com.alacarte.mstpuserbackend.utils.InvalidCredentialException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/login")
    ResponseEntity login(@RequestBody LoginForm loginForm) {
        try {
            String apiKey = userService.login(loginForm);
            return ResponseEntity.ok(apiKey);
        } catch (InvalidCredentialException ice) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(ice.getMessage());
        }
    }

    //Create a user
    @PostMapping("/signup")
    ResponseEntity createUser(@RequestBody User user){
        try {
            User usr = userService.create(user);
            return ResponseEntity.ok(usr);
        } catch (InvalidCredentialException ice) {
            return ResponseEntity.badRequest().body(ice.getMessage());
        }
    }

    @GetMapping("/get")
    ResponseEntity getUserByApiKey(@RequestHeader(name = "x-api-key") String apiKey){
        Optional<User> user = userService.getByApiKey(apiKey);
        if (user.isPresent()) {
            return ResponseEntity.ok(user.get());
        } else {
            return ResponseEntity.ok().build();
        }
    }

    //restrict this method before it goes into production
    @GetMapping("/getAll/F728A200258ABDB10FE2AE49338877F1")
    ResponseEntity<List<User>> getAllUsers(){
        return ResponseEntity.ok(userService.getAll());
    }

    @PutMapping("/update")
    ResponseEntity updateUserInfo(@RequestHeader(name = "x-api-key") String apiKey,
                                  @RequestParam(required = false) String name,
                                  @RequestParam(required = false) String email,
                                  @RequestParam(required = false) String number) {
        Optional<User> user = userService.update(apiKey, name, email, number);
        if (user.isPresent()) {
            return ResponseEntity.ok(user.get());
        } else {
            return ResponseEntity.ok().build();
        }
    }

    @DeleteMapping("/delete")
    ResponseEntity delete(@RequestHeader(name = "x-api-key") String apiKey){
        log.warn("deleting user");
        userService.delete(apiKey);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/authenticate/{apiKey}")
    ResponseEntity<String> findByApiKey(@PathVariable String apiKey) {
        String userId = userService.authenticateApiKey(apiKey);
        return ResponseEntity.ok(userId);
    }
}
