package com.alacarte.mstpuserbackend;

import com.alacarte.mstpuserbackend.utils.InvalidCredentialException;
import com.alacarte.mstpuserbackend.utils.Util;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    //Create user
    public User create(User user) throws InvalidCredentialException {
        String email = user.getEmail();
        if (userRepository.existsByEmail(email)) {
            throw new InvalidCredentialException("User with email exists!");
        } else {
            user.setSecret();
            return userRepository.save(user);
        }
    }

    //Retrieve all users
    public List<User> getAll(){
        return userRepository.findAll();
    }

    //Retrieve user by userId
    public Optional<User> getByApiKey(String ApiKey){
        return userRepository.findByApiKey(ApiKey);
    }

    //Update operation
    public Optional<User> update(String apiKey, String name, String email, String number){
        Optional<User> user = userRepository.findByApiKey(apiKey);
        if (user.isPresent()) {
            User usr = user.get();
            if (name != null) usr.setName(name);
            if (email !=null) usr.setEmail(email);
            if (number != null) usr.setNumber(number);
            userRepository.save(usr);
            return Optional.of(usr);
        } else {
            return Optional.ofNullable(null);
        }
    }

    //Delete all users - method should be restricted.
    /*
    public void deleteAll(){
        userRepository.deleteAll();
    }
     */

    //Delete a user
    public void delete(String apiKey){
        Optional<User> user = userRepository.findByApiKey(apiKey);
        if (user.isPresent()) {
            userRepository.delete(user.get());
        }
    }

    /**
     * Authentication for market data access
     * @param apiKey
     * @return String
     */
    public String authenticateApiKey(String apiKey) {
        Optional<User> user = userRepository.findByApiKey(apiKey);
        if (user.isPresent()) {
            return user.get().getId();
        } else {
            return "";
        }
    }

    /**
     * login
     */
    public String login(LoginForm loginForm) throws InvalidCredentialException {
        if (userRepository.existsByEmail(loginForm.getEmail())) {
            User user = userRepository.findByEmail(loginForm.getEmail());
            String password = user.getPassword();
            boolean match = Util.hashString(loginForm.getPassword(),user.getSalt()).equals(password);
            if (match) return user.getApiKey();
        }
        throw new InvalidCredentialException("Invalid email/password supplied!");
    }


}
