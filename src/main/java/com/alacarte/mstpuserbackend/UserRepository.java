package com.alacarte.mstpuserbackend;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends MongoRepository<User, String> {
    Optional<User> findByApiKey(String apiKey);
    User findByEmail(String email);
    Boolean existsByEmail(String email);
}
