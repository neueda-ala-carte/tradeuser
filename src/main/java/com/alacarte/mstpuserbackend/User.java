package com.alacarte.mstpuserbackend;

import com.alacarte.mstpuserbackend.utils.Util;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

@Document
@Data
public class User {

    @Id
    private String id;
    private String name;
    private String email;
    private String number;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String apiKey;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @JsonIgnore
    private String salt;

    public User() {}

    public User(String name, String email, String password, String number) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.number = number;
    }

    public void setSecret() {
        String uniqueString = this.email.concat(this.number);
        this.salt = Util.toHexString(Util.generateSalt());
        this.apiKey = Util.hashString(uniqueString, this.salt);
        this.password = Util.hashString(this.password, this.salt);
    }

}
